FROM java:8-alpine

LABEL maintainer="Jack Peng <cnjack.peng@foxmail.com>"

COPY target/ibp-hystrix-dashbord.jar /usr/local

EXPOSE 9020
ENTRYPOINT java $JAVA_OPTS -jar "/usr/local/ibp-hystrix-dashbord.jar"
