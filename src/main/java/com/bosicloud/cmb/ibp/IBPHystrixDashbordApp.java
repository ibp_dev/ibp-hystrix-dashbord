package com.bosicloud.cmb.ibp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;


/**
 * @author Jack Peng
 */
@EnableHystrixDashboard
@SpringBootApplication
public class IBPHystrixDashbordApp {

    public static void main(String[] args) {

        SpringApplication.run(IBPHystrixDashbordApp.class, args);

    }

    @Controller
    class DefaultController {

        @GetMapping({"/", ""})
        public String index(HttpServletResponse response) {
            return "redirect:/hystrix";
        }

    }

}
